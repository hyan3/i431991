/**
 * DA_Can_Com_Master.java
 */

// For test https://gitlab.zendesk.com/agent/tickets/471621

package da;

import java.sql.*;
import java.util.*;
import common.*;

/**
 * キャンペーン商品関連マスタの検索・更新を行う <p>
 * 説明:         <p>
 * @author  H.Gotoh
 * @version 1.0 2002/03/15
 */
public class DA_Can_Com_Master extends SuperDA{

	/** キャンペーン商品関連マスタ　検索SQL */
	private static final String CAN_COM_MASTER_QUERY_SQL = "SELECT "+
						" CAN_ID, "+     	  // キャンペーンＩＤ
						" COM_ID "+     	  // 商品ＩＤ
						"FROM "+
						" CAN_COM_MASTER ";         // キャンペーン商品関連マスタ

    //検索キー
    /** キーキャンペーンＩＤ */
	private String Key_Can_Id	= null;         
	/** キー商品ＩＤ */ 
	private String Key_Com_Id	= null;          

	//検索結果
	//--キャンペーン商品関連マスタ--
	/** キャンペーンＩＤ */
	private Vector Can_Com_Master_Can_Id	= new Vector();  
	/** 商品ＩＤ */ 
	private Vector Can_Com_Master_Com_Id	= new Vector();   

	/** キャンペーン商品関連マスタ取得件数 */
	private int Can_Com_Master_Rowcount      = 0;    
	/** キャンペーン商品関連マスタ実行件数 */
	private int Can_Com_Master_Execcount      = 0;    
	/** キャンペーン商品関連マスタ実行結果件数 */
	private int Can_Com_Master_Resultcount      = 0;    

	/**
	 *	デフォルトコンストラクタ
	 */
    public DA_Can_Com_Master() {
    }

	/**
	 * キャンペーン商品関連マスタ情報を取得する｡
	 * @exception	WebAppException WEBアプリケーション共通のException
	 */
	//--管理者マスタ--
	public void QueryDA() throws WebAppException
	{
		//SQLは発行せず、0件を返すよう修正
		Can_Com_Master_Rowcount = 0; 
	}
	/**
	 * キャンペーン商品関連マスタ追加処理
	 * @return	true：正常終了 ／ false：異常終了
	 * @exception	WebAppException WEBアプリケーション共通のException
	 */
	public boolean InsertDA() throws WebAppException
				
	{

		int i =0;                        // カウンタ
		Connection con = null;			 // Get conection
		PreparedStatement stmt = null;	 // Statement declare
		int count = 0;			 		 // ExecCount

		boolean	stat = false;
		//キャンペーン情報操作のSQLは追って修正予定
		String Can_Com_Master_INSERT_SQL = "insert into Can_Com_Master (" 
    				  + "CAN_ID,"
    				  + "COM_ID)"
    				  + " values(?, ?)";
		try 
		{			

			con = getDBConnection();
			stmt = con.prepareStatement(Can_Com_Master_INSERT_SQL);
			
			Can_Com_Master_Resultcount = 0;
			for (i = 0;i < getCan_Com_Master_Execcount(); i++) 
			{

				// キャンペーンID 
		        stmt.setString(1,(String)getCan_Com_Master_Can_Id().elementAt(i));
				// 商品コード              
		        stmt.setString(2,(String)getCan_Com_Master_Com_Id().elementAt(i));

				// 登録実行
				Can_Com_Master_Resultcount += stmt.executeUpdate();

			}

		}
		catch (SQLException e)
		{
			// データベース例外 WebAppExceptionにラップ
			throw new WebAppException(e, con, getClass().getName(), "InsertDA", "CAN_COM_MASTER");
		}
		catch (Exception e)
		{
			// その他例外 WebAppExceptionにラップ
			throw new WebAppException(e, con, getClass().getName(), "InsertDA");
		}
		finally
		{
			// リソース開放
			try
			{
				if (stmt != null)
				{
					stmt.close();
				}
			}
			catch (SQLException e)
			{
				// ここでは何も処理しない
			}
		}

		return true;
	}


	/**
	 * キャンペーン商品関連マスタ情報を削除する｡
	 * @return	true：正常終了 ／ false：異常終了
	 * @exception	WebAppException WEBアプリケーション共通のException
	 */
	//--キャンペーン商品関連マスタ--
	public boolean DeleteDA()  throws WebAppException
	{
		Connection con = null;			 // Get conection
		PreparedStatement stmt = null;	 // Statement declare
		int count = 0;			 		 // ExecCount
		String strSQL = null;
		String strWhere = null;
		
		boolean	stat = false;
		//キャンペーン情報操作のSQLは追って修正予定
		String Can_Com_Master_DELETE_SQL = "DELETE CAN_COM_MASTER";
		try 
		{			
			//キー：キャンペーンＩＤ
			if (!(Key_Can_Id == null) && !(Key_Can_Id.equals(""))) {
				if ( strWhere != null ) {
					strWhere += " AND";
				}
				strWhere = " CAN_ID = '" + Key_Can_Id + "'";
			}
			
			//キー：商品コード
			if (!(Key_Com_Id == null) && !(Key_Com_Id.equals(""))) {
				if ( strWhere != null ) {
					strWhere += " AND";
				}
				strWhere = " COM_ID = '" + Key_Com_Id + "'";
			}

			strSQL = Can_Com_Master_DELETE_SQL + " WHERE" + strWhere;

			con = getDBConnection();
			stmt = con.prepareStatement(strSQL);

			// 削除実行
			Can_Com_Master_Resultcount += stmt.executeUpdate();

		}
		catch (SQLException e)
		{
			// データベース例外 WebAppExceptionにラップ
			throw new WebAppException(e, con, getClass().getName(), "DeleteDA", "CAN_COM_MASTER");
		}
		catch (Exception e)
		{
			// その他例外 WebAppExceptionにラップ
			throw new WebAppException(e, con, getClass().getName(), "DeleteDA");
		}
		finally
		{
			// リソース開放
			try
			{
				if (stmt != null)
				{
					stmt.close();
				}
			}
			catch (SQLException e)
			{
				// ここでは何も処理しない
			}
		}

		return true;
	}


	/**
	 * 領域のクリアをする｡
	 * 
	 */
    public void  clear()
	{
    //検索キー
	Key_Can_Id	= null;         // キーキャンペーンＩＤ
	Key_Com_Id	= null;         // キー商品ＩＤ

	//検索結果
	Can_Com_Master_Can_Id.clear();  // キャンペーンＩＤ
	Can_Com_Master_Com_Id.clear();   // 商品ＩＤ

	Can_Com_Master_Rowcount      = 0;   // キャンペーン商品関連マスタ取得件数
	Can_Com_Master_Execcount     = 0;   // キャンペーン商品関連ﾏｽﾀ実行件数
	Can_Com_Master_Resultcount	 = 0;	// 実行結果件数
    
    }

	/** GETTER **/
	/**
	 * キャンペーンＩＤを取得する｡
	 * @return キャンペーンＩＤのVector
	 */
    public Vector getCan_Com_Master_Can_Id()
	{
        return Can_Com_Master_Can_Id;
    }

	/**
	 * 商品ＩＤを取得する｡
	 * @return 商品ＩＤのVector
	 */
    public Vector getCan_Com_Master_Com_Id()
	{
        return Can_Com_Master_Com_Id;
    }

	/**
	 * 件数を取得する｡
	 * @return	件数
	 */
    public int getCan_Com_Master_Rowcount()
	{
        return Can_Com_Master_Rowcount;
    }

	/**
	 * 実行件数を取得する｡
	 * @return 実行件数
	 */
    public int getCan_Com_Master_Execcount()
	{
        return Can_Com_Master_Execcount;
    }

	/**
	 * 実行結果件数を取得する｡
	 * @return 実行結果件数
	 */
    public int getCan_Com_Master_Resultcount()
	{
        return Can_Com_Master_Resultcount;
    }


	/** SETTER **/

	/**
	 * 検索件数(実行件数)を設定する｡
	 * @param value 検索件数
	 */
    public void setCan_Com_Master_Execcount(int value)
	{
       this.Can_Com_Master_Execcount = value;
    }

	/**
	 * キャンペーンＩＤを設定する｡
	 * @param Can_Com_Master_Can_Id キャンペーンＩＤ
	 */
    public void setCan_Com_Master_Can_Id(String Can_Com_Master_Can_Id)
	{
       this.Can_Com_Master_Can_Id.add(Can_Com_Master_Can_Id);
    }

	/**
	 * 商品ＩＤを設定する｡
	 * @param Can_Com_Master_Com_Id 商品ＩＤ
	 */
    public void setCan_Com_Master_Com_Id(String Can_Com_Master_Com_Id)
	{
       this.Can_Com_Master_Com_Id.add(Can_Com_Master_Com_Id);
    }

	/**
	 * キーキャンペーンＩＤ  を設定する｡
	 * @param Key_Can_Id キーキャンペーンＩＤ
	 */
    public void setKey_Can_Id(String Key_Can_Id)
	{
       this.Key_Can_Id = Key_Can_Id;
    }
	/**
	 * キー商品ＩＤ  を設定する｡
	 * @param Key_Com_Id キー商品ＩＤ
	 */
    public void setKey_Com_Id(String Key_Com_Id)
	{
       this.Key_Com_Id = Key_Com_Id;
    }

}
