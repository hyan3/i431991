import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
private static final String BASE_PATH = "/dg/folderpath";
public class FilePathExample {


  public static void createZipFile(String csvFilename) throws IOException {

	//File inputFile1 = new File(csvFilename);
	String fileName = extractFileName(csvFilename);
	
	File inputFile = new File(BASE_PATH+fileName);
	

	if (!inputFile.exists())
	 {
	   inputFile.createNewFile();
	 }
	//FileOutputStream fos = new FileOutputStream(inputFile+ ".zip"); 
	try (ZipOutputStream writer = new ZipOutputStream(fos))
	{
	  ZipEntry entry = new ZipEntry(inputFile.getCanonicalPath());
	  writer.putNextEntry(entry);
	} 
  }
  public static String extractFileName(String filePath) {
        int lastIndex = filePath.lastIndexOf('/');

        if (lastIndex != -1) {
            return filePath.substring(lastIndex + 1);
        } else {
            return filePath;
        }
    }
}

